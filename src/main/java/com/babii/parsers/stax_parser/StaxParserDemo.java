package com.babii.parsers.stax_parser;

import com.babii.model.Chars;
import com.babii.model.Parameters;
import com.babii.model.Plane;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class StaxParserDemo {
    private static List<Plane> planes = new ArrayList<>();
    private static Plane plane = null;
    private static Chars chars = null;
    private static Parameters parameters = null;
    private static boolean bModel;
    private static boolean bOrigin;
    private static boolean bChars;
    private static boolean bType;
    private static boolean bNumberOfSeats;
    private static boolean bCombatKit;
    private static boolean bHasRadar;
    private static boolean bParameters;
    private static boolean bLength;
    private static boolean bWidth;
    private static boolean bHeight;
    private static boolean bPrice;

    public static List<Plane> parseFile(File xml) {
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(xml));
            plane = new Plane();
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                switch (event.getEventType()) {

                    case XMLStreamConstants.START_ELEMENT:
                        StartElement startElement = event.asStartElement();
                        String qName = startElement.getName().getLocalPart();

                        if (qName.equalsIgnoreCase("Plane")) {

                        } else if (qName.equalsIgnoreCase("Model")) {
                            bModel = true;
                        } else if (qName.equalsIgnoreCase("Origin")) {
                            bOrigin = true;
                        } else if (qName.equalsIgnoreCase("Chars")) {
                            bChars = true;
                        } else if (qName.equalsIgnoreCase("type")) {
                            bType = true;
                        } else if (qName.equalsIgnoreCase("number_of_seats")) {
                            bNumberOfSeats = true;
                        } else if (qName.equalsIgnoreCase("combat_kit")) {
                            bCombatKit = true;
                        } else if (qName.equalsIgnoreCase("has_radar")) {
                            bHasRadar = true;
                        } else if (qName.equalsIgnoreCase("Parameters")) {
                            bParameters = true;
                        } else if (qName.equalsIgnoreCase("length")) {
                            bLength = true;
                        } else if (qName.equalsIgnoreCase("width")) {
                            bWidth = true;
                        } else if (qName.equalsIgnoreCase("height")) {
                            bHeight = true;
                        } else if (qName.equalsIgnoreCase("Price")) {
                            bPrice = true;
                        }
                        break;

                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();

                        if (bModel) {
                            plane.setModel(characters.getData());
                            bModel = false;
                        }
                        if (bOrigin) {
                            plane.setOrigin(characters.getData());
                            bOrigin = false;
                        }
                        if (bChars) {
                            chars = new Chars();
                            bChars = false;
                        }
                        if (bType) {
                            chars.setType(characters.getData());
                            bType = false;
                        }
                        if (bNumberOfSeats) {
                            chars.setNumberOfSeats(Integer.parseInt(characters.getData()));
                            bNumberOfSeats = false;
                        }
                        if (bCombatKit) {
                            chars.setCombatKit(characters.getData());
                            bCombatKit = false;
                        }
                        if (bHasRadar) {
                            chars.setHasRadar(Boolean.parseBoolean(characters.getData()));
                            bHasRadar = false;
                        }
                        if (bParameters) {
                            parameters = new Parameters();
                            bParameters = false;
                        }
                        if (bLength) {
                            parameters.setLength(Integer.parseInt(characters.getData()));
                            bLength = false;
                        }
                        if (bWidth) {
                            parameters.setWidth(Integer.parseInt(characters.getData()));
                            bWidth = false;
                        }
                        if (bHeight) {
                            parameters.setHeight(Integer.parseInt(characters.getData()));
                            bHeight = false;
                        }
                        if (bPrice) {
                            plane.setPrice(Integer.parseInt(characters.getData()));
                            bPrice = false;
                        }
                        plane.setChars(chars);
                        plane.setParameters(parameters);
                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        EndElement endElement = event.asEndElement();

                        if (endElement.getName().getLocalPart().equalsIgnoreCase("Plane")) {
                            planes.add(plane);
                            plane = new Plane();
                        }
                        break;
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return planes;
    }
}
