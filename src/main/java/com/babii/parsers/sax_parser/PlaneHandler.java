package com.babii.parsers.sax_parser;

import com.babii.model.Chars;
import com.babii.model.Parameters;
import com.babii.model.Plane;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.util.ArrayList;
import java.util.List;

public class PlaneHandler extends DefaultHandler {
    private List<Plane> planes = new ArrayList<>();
    private Plane plane = null;
    private Chars chars = null;
    private Parameters parameters = null;
    private boolean bModel;
    private boolean bOrigin;
    private boolean bChars;
    private boolean bType;
    private boolean bNumberOfSeats;
    private boolean bCombatKit;
    private boolean bHasRadar;
    private boolean bParameters;
    private boolean bLength;
    private boolean bWidth;
    private boolean bHeight;
    private boolean bPrice;

    public List<Plane> getPlaneList() {
        return this.planes;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        if (qName.equalsIgnoreCase("Plane")) {
            plane = new Plane();
        } else if (qName.equalsIgnoreCase("Model")) {
            bModel = true;
        } else if (qName.equalsIgnoreCase("Origin")) {
            bOrigin = true;
        } else if (qName.equalsIgnoreCase("Chars")) {
            bChars = true;
        } else if (qName.equalsIgnoreCase("type")) {
            bType = true;
        } else if (qName.equalsIgnoreCase("number_of_seats")) {
            bNumberOfSeats = true;
        } else if (qName.equalsIgnoreCase("combat_kit")) {
            bCombatKit = true;
        } else if (qName.equalsIgnoreCase("has_radar")) {
            bHasRadar = true;
        } else if (qName.equalsIgnoreCase("Parameters")) {
            bParameters = true;
        } else if (qName.equalsIgnoreCase("length")) {
            bLength = true;
        } else if (qName.equalsIgnoreCase("width")) {
            bWidth = true;
        } else if (qName.equalsIgnoreCase("height")) {
            bHeight = true;
        } else if (qName.equalsIgnoreCase("Price")) {
            bPrice = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("Plane")) {
            planes.add(plane);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (bModel) {
            plane.setModel(new String(ch, start, length));
            bModel = false;
        } else if (bOrigin) {
            plane.setOrigin(new String(ch, start, length));
            bOrigin = false;
        } else if (bChars) {
            chars = new Chars();
            bChars = false;
        } else if (bType) {
            chars.setType(new String(ch, start, length));
            bType = false;
        } else if (bNumberOfSeats) {
            chars.setNumberOfSeats(Integer.parseInt(new String(ch, start, length)));
            bNumberOfSeats = false;
        } else if (bCombatKit) {
            chars.setCombatKit(new String(ch, start, length));
            bCombatKit = false;
        } else if (bHasRadar) {
            chars.setHasRadar(Boolean.parseBoolean(new String(ch, start, length)));
            bHasRadar = false;
        } else if (bParameters) {
            parameters = new Parameters();
            bParameters = false;
        } else if (bLength) {
            parameters.setLength(Integer.parseInt(new String(ch, start, length)));
            bLength = false;
        } else if (bWidth) {
            parameters.setWidth(Integer.parseInt(new String(ch, start, length)));
            bWidth = false;
        } else if (bHeight) {
            parameters.setHeight(Integer.parseInt(new String(ch, start, length)));
            bHeight = false;
        } else if (bPrice) {
            plane.setPrice(Integer.parseInt(new String(ch, start, length)));
            bPrice = false;
            plane.setChars(chars);
            plane.setParameters(parameters);
        }
    }
}
