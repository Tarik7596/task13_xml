package com.babii.parsers.dom_parser;

import com.babii.model.Chars;
import com.babii.model.Parameters;
import com.babii.model.Plane;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.ArrayList;
import java.util.List;

public class DomDocReader {
    List<Plane> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Plane> planes = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("Plane");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Plane plane = new Plane();
            Chars chars;
            Parameters parameters;
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                plane.setModel(element.getElementsByTagName("Model").item(0).getTextContent());
                plane.setOrigin(element.getElementsByTagName("Origin").item(0).getTextContent());
                plane.setPrice(
                        Integer.parseInt(element.getElementsByTagName("Price").item(0).getTextContent()));
                chars = getChars(element.getElementsByTagName("Chars"));
                parameters = getParameter(element.getElementsByTagName("Parameters"));
                plane.setChars(chars);
                plane.setParameters(parameters);

                planes.add(plane);
            }
        }
        return planes;
    }

    private Chars getChars(NodeList nodes) {
        Chars chars = new Chars();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            chars.setType(element.getElementsByTagName("type").item(0).getTextContent());
            chars.setNumberOfSeats(Integer
                    .parseInt(element.getElementsByTagName("number_of_seats").item(0).getTextContent()));
            chars.setCombatKit(element.getElementsByTagName("combat_kit").item(0).getTextContent());
            chars.setHasRadar(
                    Boolean.parseBoolean(element.getElementsByTagName("has_radar").item(0).getTextContent()));
        }
        return chars;
    }

    private Parameters getParameter(NodeList nodes) {
        Parameters parameters = new Parameters();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodes.item(0);
            parameters.setLength(
                    Integer.parseInt(element.getElementsByTagName("length").item(0).getTextContent()));
            parameters.setWidth(
                    Integer.parseInt(element.getElementsByTagName("width").item(0).getTextContent()));
            parameters.setHeight(
                    Integer.parseInt(element.getElementsByTagName("height").item(0).getTextContent()));
        }

        return parameters;
    }
}
