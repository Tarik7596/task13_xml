package com.babii.parsers.dom_parser;

import java.io.File;
import java.io.IOException;
import java.util.List;
import com.babii.model.Plane;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class DomParserPlane {
    public static List<Plane> getPlaneList(File xml, File xsd){
        DomDocCreator creator = new DomDocCreator(xml);
        Document doc = creator.getDocument();
        try {
            DomValidator.validate(DomValidator.createSchema(xsd),doc);
        }catch (IOException | SAXException ex){
            ex.printStackTrace();
        }
        DomDocReader reader = new DomDocReader();
        return reader.readDoc(doc);
    }
}
