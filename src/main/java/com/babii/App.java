package com.babii;

import com.babii.parsers.dom_parser.DomParserPlane;
import com.babii.parsers.sax_parser.SAXParserPlane;
import com.babii.parsers.stax_parser.StaxParserDemo;

import java.io.File;

public class App {
    public static void main(String[] args) {
        File file1 = new File("src/main/resources/plane.xml");
        File file2 = new File("src/main/resources/plane.xsd");
        //Dom parser
        DomParserPlane.getPlaneList(file1, file2)
                .forEach(System.out::println);
        //Sax parser\
        SAXParserPlane.parsePlanes(file1, file2)
                .forEach(System.out::println);
        //Stax parser
        StaxParserDemo.parseFile(file1).forEach(System.out::println);
    }
}
